const fs = require("fs");

const reqListener = (req, res) => {
  // console.log(req.url, req.method, req.headers);
  const { url, method } = req;
  // process.exit();
  if (url === "/") {
    res.setHeader("Content-Type", "text/html");
    res.write("<html>");
    res.write("<head><title>Hello</title></head>");
    res.write(
      "<body><form action='/message' method='POST'><input name='inputMessage'/><button>SEND</button></form></body>"
    );
    res.write("</html>");
    return res.end();
  }
  if (url === "/users") {
    let content = "";
    return fs.readFile("message.txt", "utf8", (err, data) => {
      if (err) content = "no data :(";
      console.log(data);
      const userList = data
        .split(",")
        .map((userName) => `<li>${userName}</li>`);
      console.log("userList", userList.join(""));
      content = `<ul>${userList.join("")}</ul>`;
      res.setHeader("Content-Type", "text/html");
      res.write("<html>");
      res.write("<head><title>Hello</title></head>");
      res.write(
        `<body>
              <h1>Users:</h1>
              ${content}
              <br/>
              <br/>
              <form action='/new-user' method='POST'><input name='newUser'/><button>ADD USER</button></form>
          </body>`
      );
      res.write("</html>");
      return res.end();
    });
  }
  if (url === "/message" && method === "POST") {
    const body = [];
    req.on("data", (chunk) => {
      console.log("chunk");
      body.push(chunk);
    });
    return req.on("end", () => {
      const parsedBody = Buffer.concat(body).toString();
      const message = parsedBody.split("=")[1];
      console.log("parsedBody", parsedBody);
      fs.writeFile("message.txt", message, () => {
        res.statusCode = 302;
        res.setHeader("Location", "/");
        return res.end();
      });
    });
  }
  if (url === "/new-user" && method === "POST") {
    console.log("NEW-USER");
    const postData = [];
    req.on("data", (chunk) => {
      console.log("chunk");
      postData.push(chunk);
    });
    return req.on("end", () => {
      const parsedPostData = Buffer.concat(postData).toString();
      console.log("parsedPostData", parsedPostData);
      const newUser = parsedPostData.split("=")[1];
      console.log("newUser", newUser);
      return fs.readFile("message.txt", "utf8", (err, data) => {
        if (err) throw err;
        console.log(data);
        return fs.writeFile("message.txt", `${data},${newUser}`, () => {
          res.statusCode = 302;
          res.setHeader("Location", "/users");
          return res.end();
        });
      });
    });
  }

  res.setHeader("Content-Type", "text/html");
  res.write("<html>");
  res.write("<head><title>Hello</title></head>");
  res.write("<body><h1>Hello</h1></body>");
  res.write("</html>");
  res.end();
};

// module.exports = reqListener;

module.exports = {
  listener: reqListener,
  someText: "BLA BLA BLA",
};
